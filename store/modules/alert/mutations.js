export const types = {
    SET_ALERT: 'SET_ALERT'
};

function deleteTimeout() {
    clearTimeout(timeout);
}

export const mutations = {
    [types.SET_ALERT](state, payload) {
        state.alert = {
            show: true,
            text: payload.text,
            type: payload.type
        };
        setTimeout(() => {
            state.alert.show = false;
        }, 3000);
    }
};
