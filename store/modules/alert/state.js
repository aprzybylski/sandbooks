import { mutations } from './mutations';
import actions from './actions';

export default {
    namespaced: true,
    state: {
        alert: {
            show: false,
            type: null,
            text: null
        }
    },
    mutations,
    actions
};
