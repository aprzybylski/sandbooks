import { types } from './mutations';

export default {
    setAlert({ commit }, payload) {
        commit(types.SET_ALERT, payload);
    }
};