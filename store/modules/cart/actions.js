import { types } from './mutations';

export default {
    addToCart({ state, commit }, item) {
        commit(types.ADD_TO_CART, item);
        commit(types.SET_TOTAL, item.price);
    },
    removeFromCart({ commit }, id) {
        commit(types.REMOVE_FROM_CART, id);
    },
    removeAllCart({ commit }) {
        commit(types.REMOVE_ALL_CART);
    }
};
