export const types = {
    ADD_TO_CART: 'ADD_TO_CART',
    SET_TOTAL: 'SET_TOTAL',
    REMOVE_FROM_CART: 'REMOVE_FROM_CART',
    REMOVE_ALL_CART: 'REMOVE_ALL_CART'
};

export const mutations = {
    [types.ADD_TO_CART](state, item) {
        if (!state.cart[item.id]) {
            state.cart[item.id] = item;
            state.cart[item.id].qty = 1;
        } else {
            state.cart[item.id].qty += 1;
        }
    },
    [types.SET_TOTAL](state, price) {
        state.total += price;
    },
    [types.REMOVE_FROM_CART](state, id) {
        state.total = state.total - state.cart[id].qty * state.cart[id].price;
        delete state.cart[id];
    },
    [types.REMOVE_ALL_CART](state) {
        state.cart = {};
        state.total = 0;
    }
};
