export const types = {
    SET_EBOOKS: 'SET_EBOOKS',
    SET_EBOOK: 'SET_EBOOK'
};

export const mutations = {
    [types.SET_EBOOKS](state, ebooks) {
        state.ebooks = ebooks;
    },
    [types.SET_EBOOK](state, ebook) {
        state.ebook = ebook;
    }
};
