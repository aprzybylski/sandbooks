import { mutations } from './mutations';
import actions from './actions';

export default {
    namespaced: true,
    state: {
        ebooks: [],
        ebook: null
    },
    mutations,
    actions
};
