import { types } from './mutations';

export default {
    setBooks({ commit }, ebooks) {
        commit(types.SET_EBOOKS, ebooks);
    },
    setBook({ commit }, ebook) {
        commit(types.SET_EBOOK, ebook);
    }
};
