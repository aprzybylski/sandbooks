import authentication from './authentication/state';
import books from './books/state';
import cart from './cart/state';
import alert from './alert/state';
import ebooks from './ebooks/state';

export default {
    authentication,
    books,
    cart,
    alert,
    ebooks
};
