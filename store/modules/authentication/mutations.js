export const types = {
    LOGIN: 'LOGIN'
};

export const mutations = {
    [types.LOGIN](state, email) {
        state.logged = email;
    }
};
