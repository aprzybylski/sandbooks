import { types } from './mutations';
import axios from '~/plugins/axios';
import Cookie from 'js-cookie';

export default {
    login({ commit }, payload) {
        return axios
            .post('/login', {
                email: payload.email,
                password: payload.password
            })
            .then(response => {
                if (response.data.status) {
                    commit(types.LOGIN, payload.email);
                    Cookie.set('isLogged', payload.email);
                } else {
                    commit(types.LOGIN, false);
                }
            })
            .catch(error => {
                console.log(error);
            });
    },
    register({ commit }, payload) {
        return axios
            .post('/register', {
                email: payload.email,
                password: payload.password
            })
            .then(response => {
                if (response.data.status) {
                    commit(types.LOGIN, payload.email);
                    Cookie.set('isLogged', payload.email);
                } else {
                    commit(types.LOGIN, false);
                }
            })
            .catch(error => {
                console.log(error);
            });
    },
    logout({ commit }) {
        commit(types.LOGIN, false);
        Cookie.remove('isLogged');
    },
    initAuth({ commit }) {
        if (Cookie.get('isLogged')) {
            commit(types.LOGIN, Cookie.get('isLogged'));
            return true;
        } else {
            return false;
        }
    }
};
