import { mutations } from './mutations';
import actions from './actions';

export default {
    namespaced: true,
    state: {
        books: [],
        book: null
    },
    mutations,
    actions
};
