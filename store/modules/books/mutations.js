export const types = {
    SET_BOOKS: 'SET_BOOKS',
    SET_BOOK: 'SET_BOOK'
};

export const mutations = {
    [types.SET_BOOKS](state, books) {
        state.books = books;
    },
    [types.SET_BOOK](state, book) {
        state.book = book;
    }
};
