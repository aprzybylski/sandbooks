import { types } from './mutations';

export default {
    setBooks({ commit }, books) {
        commit(types.SET_BOOKS, books);
    },
    setBook({ commit }, book) {
        commit(types.SET_BOOK, book);
    }
};
