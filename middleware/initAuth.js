export default function(context) {
    context.store.dispatch('authentication/initAuth').then(response => {
        if (response) {
            if (context.route.name === '/authentication') {
                context.redirect('/');
            }
        } else if (context.route.name !== 'authentication') {
            context.redirect('/authentication');
        }
    });
}
