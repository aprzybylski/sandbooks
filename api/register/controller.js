Register = require('./model');

exports.register = function(req, res) {
    var register = new Register();
    Register.findOne(
        {
            email: req.body.email
        },
        function(err, user) {
            if (user) {
                res.json({
                    message: 'this user already exist',
                    status: false
                });
            } else {
                register.email = req.body.email;
                register.password = req.body.password;
                register.save(function() {
                    res.json({
                        message: 'New user created!',
                        data: register,
                        status: true
                    });
                });
            }
        }
    );
};
