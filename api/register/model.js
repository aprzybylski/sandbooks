var mongoose = require('mongoose');
// Setup schema
var registerSchema = mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

var Contact = module.exports = mongoose.model('user', registerSchema);
module.exports.get = function(callback, limit) {
    Contact.find(callback).limit(limit);
};
