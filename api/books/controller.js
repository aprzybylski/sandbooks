Books = require('./model');
// Handle index actions
exports.index = function(req, res) {
    Books.get(function(err, books) {
        if (err) {
            res.json({
                status: false,
                message: err
            });
        }
        res.json({
            status: true,
            data: books
        });
    });
};

exports.view = function(req, res) {
    Books.findOne(
        {
            id: parseInt(req.params.id)
        },
        function(err, book) {
            if (err) {
                res.send(err);
            }
            res.json({
                status: true,
                data: book
            });
        }
    );
};
