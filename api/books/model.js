var mongoose = require('mongoose');
// Setup schema
var BooksSchema = mongoose.Schema({});

var Books = module.exports = mongoose.model('book', BooksSchema);
module.exports.get = function(callback, limit) {
    Books.find(callback).limit(limit);
};
