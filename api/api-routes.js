// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function(req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to sandbooks crafted with love!'
    });
});
// Import contact controller
var loginController = require('./login/controller'),
    registerController = require('./register/controller'),
    booksController = require('./books/controller'),
    ebooksController = require('./ebooks/controller');
// Contact routes
router.route('/login').post(loginController.login);
router.route('/register').post(registerController.register);
router.route('/books').get(booksController.index);
router.route('/books/:id').get(booksController.view);
router.route('/ebooks').get(ebooksController.index);
router.route('/ebooks/:id').get(ebooksController.view);

// Export API routes
module.exports = router;
