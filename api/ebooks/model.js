var mongoose = require('mongoose');
// Setup schema
var EbooksSchema = mongoose.Schema({});

var Ebooks = module.exports = mongoose.model('ebook', EbooksSchema);
module.exports.get = function(callback, limit) {
    Ebooks.find(callback).limit(limit);
};
