Ebooks = require('./model');
// Handle index actions
exports.index = function(req, res) {
    Ebooks.get(function(err, ebooks) {
        if (err) {
            res.json({
                status: false,
                message: err
            });
        }
        res.json({
            status: true,
            data: ebooks
        });
    });
};

exports.view = function(req, res) {
    Ebooks.findOne(
        {
            id: parseInt(req.params.id)
        },
        function(err, ebook) {
            if (err) {
                res.send(err);
            }
            res.json({
                status: true,
                data: ebook
            });
        }
    );
};
